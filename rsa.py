import random


def miller_rabin(n, k=10):
    if n == 2 or n == 3:
        return True
    if n < 2 or n % 2 == 0:
        return False

    # Escribir n - 1 como 2^r * d
    r = 0
    d = n - 1
    while d % 2 == 0:
        r += 1
        d //= 2

    # Realizar k iteraciones del test
    for _ in range(k):
        a = random.randint(2, n - 2)
        x = pow(a, d, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

# Función para generar un número primo aleatorio usando Miller-Rabin
def generate_prime(bits):
    while True:
        p = random.getrandbits(bits)
        # Asegurar que el número generado sea impar
        p |= (1 << bits - 1) | 1
        if miller_rabin(p):
            return p

#Para calcular MCD
def euclides(a, b):
    while b != 0:
        a, b = b, a % b
    return a

def euclides_extendido(a, b):
    if a == 0:
        return 0, 1
    else:
        x, y = euclides_extendido(b % a, a)
        return int(y - (b // a) * x), int(x)
    
def square_and_multiply(base, exponent, modulo):
    result = 1
    base = base % modulo

    while exponent > 0:
        if exponent % 2 == 1:
            result = (result * base) % modulo
        exponent //= 2
        base = (base * base) % modulo

    return result

def factorizar_en_primos(n):
    factores_primos = []
    # Dividir por 2 hasta que sea impar
    while n % 2 == 0:
        factores_primos.append(2)
        n = n // 2
    # Buscar factores primos impares
    for i in range(3, int(n**0.5)+1, 2):
        while n % i == 0:
            factores_primos.append(i)
            n = n // i
    # Si n es primo mayor que 2
    if n > 2:
        factores_primos.append(n)
    return factores_primos

def funcion_phi(m):
    factores = factorizar_en_primos(m)
    # print(factores)
    resultado = m
    for p in set(factores):
        resultado *= (p - 1) / p
    return int(resultado)
def funcion_phi(m):
    factores = factorizar_en_primos(m)
    resultado = m
    for p in set(factores):
        resultado *= (p - 1) / p
    return int(resultado)


def calcular_e(phi, p, q):
    r = (p - 1) * (q - 1)
    candidates = []
    for e in range(2, phi):
        if euclides(e, phi) == 1 and euclides(e, r) == 1:
            candidates.append(e)
    return random.choice(candidates) if candidates else None

def iniciar_variables(bits):
    p = generate_prime(bits)
    q = generate_prime(bits)
    n = p * q
    phi = funcion_phi(n)
    e = calcular_e(phi, p, q)
    d = square_and_multiply(e, -1, phi)
    return p, q, n, phi, e, d
    
###########
def mensaje_a_numero(message):
    return [ord(char) for char in message]

def cifrado(message, public_key):
    n, e = public_key
    numbers = mensaje_a_numero(message)
    encrypted_numbers = [square_and_multiply(num, e, n) for num in numbers]
    return encrypted_numbers

def descifrado(encrypted_numbers, private_key):
    n, d = private_key
    decrypted_numbers = [square_and_multiply(num, d, n) for num in encrypted_numbers]
    return numero_a_mensaje(decrypted_numbers)

def numero_a_mensaje(numbers):
    return ''.join(chr(num) for num in numbers)


p, q, n, phi, e, d = iniciar_variables(11)
print(f"p: {p}")
print(f"q: {q}")
print(f"n: {n}")
print(f"phi: {phi}")
print(f"e: {e}")
print(f"d: {d}")



public_key = [n, e]
private_key = [n, d]
print(f"Llave pública: {public_key}")
print(f"Llave privada: {private_key}")

message = "Hola Mundo jaja 123"

# Cifrar el mensaje
encrypted_numbers = cifrado(message, public_key)
print(f"Mensaje cifrado: {encrypted_numbers}")

# Descifrar el mensaje
decrypted_message = descifrado(encrypted_numbers, private_key)
print(f"Mensaje descifrado: {decrypted_message}")



